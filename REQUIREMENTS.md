# Interview Coding Exercise
Our business users require a new microservice to manage decks of playing cards. This service will be used to create decks 
of cards, manage their shuffle state, and deal cards.

Implement the requested features of the Card Shuffling API below. 

As stated in your interview you are not expected to finish all requirements. Please spend no more than 2 hours on this assignment.

## Business Requirements
1. As an api user, I need to create a new card deck in "ace-high" order.
   1. A deck of cards should contain 54 cards (52 ranked cards + 2 Jokers) 
   2. All newly created decks should be sorted in the following order by default, unless otherwise specified:
      1. Aces - spades, clubs, diamonds, hearts
      2. Kings - spades, clubs, diamonds, hearts
      3. Queens - spades, clubs, diamonds, hearts
      4. Jacks - spades, clubs, diamonds, hearts
      5. Number Cards - Descending order starting at 9 in the same suit order as the previous (spades, clubs, diamonds, hearts)
      6. Jokers (If included)
2. All card decks must be identified by a unique identifier which can be used to access the card deck via the api.
3. As an api user, when creating a new deck of cards I would like to be able to specify if the deck should be shuffled.
4. As an api user, when creating a new deck of cards I would like Jokers to be excluded by default.
5. As an api user, when creating a new deck of cards I would like to specify if Jokers should be included.
6. As an api user, I need to be able to shuffle an existing deck.
   1. This shall shuffle all cards, dealt and un-dealt, and return all cards to the un-dealt state.
7. As an api user, I need to be able to retrieve all cards in the deck sorted into two buckets (dealt and un-dealt).
8. As an api user, I need to be able to retrieve the next card from an existing shuffled card deck.
9. As an api user, I expect that when I deal a card from a deck it is moved from un-dealt state to the dealt state.
10. As an api user, I expect that dealt cards cannot be re-dealt without shuffling the deck.
11. As an api user, when retrieving the next card from an existing deck and there are no mor un-dealt cards I expect to receive a 404 and appropriate notification to reshuffle the deck.

## Technical Requirements
1. The API must be implemented as a RESTful web service.
2. The API must be implemented using Spring Boot.
3. The API must be built using Gradle.
4. The reviewer must be able to run the application using a single command. By default, this will be the standard `bootRun` command provided by the Spring Boot Gradle Plugin. If you wish to change this please make note in the README.
5. You must provide a sample curl command for each endpoint you implement in this exercise. Please place these in a `SAMPLES.md` file in the root directory.
