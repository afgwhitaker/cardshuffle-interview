# cardshuffle-interview
Interview coding exercise to create a card shuffling application.

This exercise is intended for you to showcase how you design and build RESTful web services.

## Instructions
* Implement the Card Shuffling API features specified in the [REQUIREMENTS](REQUIREMENTS.md) on a new branch named `solution`.
* As stated in your interview, you are not expected to finish the exercise.
* Please spend no more than 2 hours on the exercise.
* When you are done with the exercise please make a merge request from your `solution` branch to the `main` branch.

## Requirements
Business and technical requirements for this exercise can be found in the [REQUIREMENTS](REQUIREMENTS.md) document in
the root directory.

## License
Proprietary and Confidential

Copyright 2022 American Family Insurance, Inc. All Rights Reserved.

Unauthorized copying or use of this software, via any medium, is strictly prohibited.
